package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.service.IAbstractService;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        findById(id);
        return true;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
