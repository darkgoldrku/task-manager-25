package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Project;

public interface IProjectService extends IAbstractUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description);

    @NotNull
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description);

    @NotNull
    Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description);

    @NotNull
    Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @NotNull final Status status);

}
