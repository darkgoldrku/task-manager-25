package ru.t1.bugakov.tm.api.comparator;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    @NotNull Date getCreated();

    void setCreated(@NotNull final Date created);

}
