package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
